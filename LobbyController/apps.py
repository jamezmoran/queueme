from django.apps import AppConfig


class LobbycontrollerConfig(AppConfig):
    name = 'LobbyController'
