from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden
from django.template import loader

from .models import Lobby, Player
from django.core.urlresolvers import reverse
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout
from django.shortcuts import redirect

def get_player(user):
	return Player.objects.get(user=user)

def index(request):
	lobbies = Lobby.objects.all()
	template = loader.get_template('lobbies/index.html')
	context = {
		'lobbies' : lobbies
	}
	if request.user.is_authenticated():
		context['curr_player'] = get_player(request.user)
	return HttpResponse(template.render(context,request))

def logout_session(request):
	logout(request)
	return redirect(reverse('lobbies:index'))
	
@login_required
def kick_player(request):
	lobbyId = request.GET['lobbyId']
	kickedPlayerId = request.GET['playerId']
	lobby = Lobby.objects.get(id=lobbyId)
	kickedPlayer = Player.objects.get(id=kickedPlayerId)
	self = Player.objects.get(user=request.user)
	if lobby.lobbyLeader == self:
		if self != kickedPlayer and kickedPlayer.currentLobby == lobby:
			kickedPlayer.currentLobby = None
			kickedPlayer.save()
	else:
		return HttpResponseForbidden("You cannot kick that player in that lobby.")
	return redirect(request.META.get('HTTP_REFERER'))

@login_required
def create_lobby(request):
	lobbyId = Lobby.create_lobby(get_player(request.user), request.POST['title'])
	return HttpResponseRedirect(reverse('lobbies:lobby',args=[lobbyId]))

@login_required
def join_lobby(request, lobbyId):
	lobbies = Lobby.objects.filter(id=lobbyId)
	if lobbies:
		lobby = lobbies[0]
		player = get_player(request.user)
		currentLobby = player.currentLobby
		if currentLobby:
			if currentLobby == lobby:
				return redirect(reverse('lobbies:lobby',args=[lobbyId]))
			elif currentLobby.lobbyLeader == player:
				currentLobby.delete()
		player.currentLobby = lobby
		player.save()
		return redirect(reverse('lobbies:lobby',args=[lobbyId]))
	else:
		return Http404("Not found")

@login_required
def leave_lobby(request):
	player = get_player(request.user)
	if player.currentLobby and player.currentLobby.lobbyLeader == player:
		player.currentLobby.delete()
	player.currentLobby = None
	player.save()
	return redirect(reverse('lobbies:index'))
		
@login_required
def lobby(request, lobbyId):
	lobbyEntry = Lobby.objects.get(id=lobbyId)
	players = Player.objects.filter(currentLobby=lobbyId)
	template = loader.get_template('lobbies/lobby.html')
	context = {}
	context = {
		'lobby' : lobbyEntry,
		'players' : players
	}
	context['curr_player'] = Player.objects.get(user=request.user)
		
	return render(request, 'lobbies/lobby.html', context)
