from django.conf.urls import url
from django.contrib.auth import login, logout

from . import views

app_name = 'lobbies'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^create_lobby/$', views.create_lobby, name='create_lobby'),
	url(r'^leave_lobby/$', views.leave_lobby, name='leave_lobby'),
	url(r'^kick_player/$', views.kick_player, name='kick_player'),

	url(r'^join_lobby/(?P<lobbyId>\d+)/$', views.join_lobby, name='join_lobby'),
	url(r'^(?P<lobbyId>\d+)/$', views.lobby, name='lobby'),
	url(r'^login/$', views.lobby, name='login'),
	url(r'^logout/$', views.logout_session, name='logout_session'),
]
