from channels import Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http
from django.db.models import Q
from django.db import transaction
from .models import Player, Lobby
import json
import html

def get_player(user):
	return Player.objects.get(user=user)

def handleChatMessage(user, reply_channel, lobbyId, message):
	Group('users_'+str(lobbyId)).send({
		'text' : json.dumps({
			'type' : 'chat',
			'payload' : {
				'user' : str(user),
				'text' : html.escape(message['text'])
			}
		})
	}),

def handleJoinLobbyMessage(user, reply_channel, lobbyId, message):
	with transaction.atomic():
		player = Player.objects.using('serializable').get(user=user)
		playerCount = Player.objects.using('serializable').filter(currentLobby=lobbyId).count()
		if playerCount < 5:
			if player.currentLobby:
				currentLobby_id = player.currentLobby.id
				if player == player.currentLobby.lobbyLeader and currentLobby_id != int(lobbyId):
					player.currentLobby.delete()
					Group('users_%s' % currentLobby_id).send({
						'text' : json.dumps({
							'type' : 'notice',
							'payload' : {
								'text' : 'Lobby leader has left the lobby'
							}
						})
					})
				player.currentLobby = None
				update_lobby(currentLobby_id)
			player.currentLobby = Lobby.objects.using('serializable').get(id=lobbyId)
			player.save()
			update_lobby(lobbyId)
		else:
			response = {
				'type' : 'notice',
				'payload' : {
					'text' : "Max players already reached in lobby"
				}
			}
			reply_channel.send({
				'text' : json.dumps(response)
			})


messageHandlers = {
	'chat' : handleChatMessage,
	'joinLobby' :	handleJoinLobbyMessage
}

def update_lobby(lobbyId):
	players = Player.objects.filter(currentLobby=lobbyId)
	players_list = []
	for p in players:
		players_list.append({
			'nickname' : p.nickname,
			'rating' : p.rating,
			'rank' : p.rank,
			'avatar' : p.avatar
		})

	Group('users_'+str(lobbyId)).send({ 
		'text': json.dumps({
			'type' : 'players',
			'payload' : {
				'players' : players_list 
			}
		})
	})


@channel_session_user_from_http
def ws_connect(message, lobbyId):
	message.reply_channel.send({'accept':True})
	Group('users_'+str(lobbyId)).add(message.reply_channel)
	update_lobby(lobbyId)


@channel_session_user
def ws_receive(message, lobbyId):
	user = message.user
	print(message)
	print("received %s"%message['text'])
	if user.is_authenticated():
		received = json.loads(message['text'])
		messageHandlers[received['type']](user, message.reply_channel, lobbyId, received['payload'])

@channel_session_user
def ws_disconnect(message, lobbyId):
	Group('users_'+str(lobbyId)).discard(message.reply_channel)
	player = get_player(message.user)
	if player.currentLobby.id == int(lobbyId):
		print("yes")
		if player.currentLobby.lobbyLeader == player:
			player.currentLobby.delete()
			Group('users_%s' % str(lobbyId)).send({
				'text' : json.dumps({
					'type' : 'notice',
					'payload' : {
						'text' : 'Lobby leader has left the lobby'
					}
				})
			})
		player.currentLobby = None
		player.save()
		update_lobby(lobbyId)
