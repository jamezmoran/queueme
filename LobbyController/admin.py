from django.contrib import admin

# Register your models here.

from .models import Player, Lobby

admin.site.register(Player)
admin.site.register(Lobby)
