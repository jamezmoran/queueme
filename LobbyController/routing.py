from channels.routing import route, include
from .consumers import ws_connect, ws_receive, ws_disconnect


channel_routing = [
    route('websocket.connect', ws_connect, path=r'(?P<lobbyId>\d+)/'),
    route('websocket.receive', ws_receive, path=r'(?P<lobbyId>\d+)/'),
    route('websocket.disconnect', ws_disconnect, path=r'(?P<lobbyId>\d+)/'),
]
