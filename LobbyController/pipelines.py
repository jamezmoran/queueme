from .models import Player
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

def get_steam_profile(uid, user, response, social, details, *args, **kwargs):
	player_details = details['player']

	player = None 
	try:
		player = Player.objects.get(url=player_details['profileurl'])
	except ObjectDoesNotExist:
		player = Player(url=player_details['profileurl'])
	player.nickname = player_details['personaname']
	player.avatar = player_details['avatarmedium']
	player.user = User.objects.get(username=user)
	player.rating = 3
	player.rank = 3
	player.rank_lastUpdated = "2011-01-01"
	player.save()
	
