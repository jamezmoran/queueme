from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
import datetime

# Create your models here.

class Player(models.Model):
	nickname = models.CharField(max_length=255)
	avatar = models.CharField(max_length=255)
	url = models.CharField(max_length=255, unique=True)
	user = models.ForeignKey(User)
	rating = models.FloatField()
	rank = models.IntegerField()
	rank_lastUpdated = models.DateTimeField()

	currentLobby = models.ForeignKey('Lobby', models.SET_NULL, blank=True, null=True)

	def __str__(self):
		return self.nickname + " : " + self.url


class Lobby(models.Model):
	startTime = models.DateTimeField('Date the lobby was started')
	lobbyTitle = models.CharField(max_length=100, null=True)
	lobbyLeader = models.ForeignKey(Player, null=True)
	seat1 = models.ForeignKey(Player, null=True, related_name='seat1_players') 
	seat2 = models.ForeignKey(Player, null=True, related_name='seat2_players')
	seat3 = models.ForeignKey(Player, null=True, related_name='seat3_players')
	seat4 = models.ForeignKey(Player, null=True, related_name='seat4_players')
	seat5 = models.ForeignKey(Player, null=True, related_name='seat5_players')

	def __str__(self):
		return str(self.id)
	
	def create_lobby(player, title):
		try:
			Lobby.objects.get(lobbyLeader=player).delete()
		except ObjectDoesNotExist:
			swallow=1
		lobby = Lobby(
			startTime=datetime.datetime.now(), 
			lobbyTitle=title, 
			lobbyLeader=player
		)
		lobby.save()
		player.currentLobby = lobby
		player.save()
		return Lobby.objects.latest('id')

