from channels.routing import route, include

channel_routing = [
    include('LobbyController.routing.channel_routing', path=r'^/lobbies/'),
]
