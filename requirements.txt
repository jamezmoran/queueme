Django==1.11.2
django-bootstrap3==8.2.3
mysqlclient==1.3.10
python-social-auth==0.3.6
channels=1.10
